# Introduction
The servicedesk-cc script processes incoming email intended for a JIRA Service Desk
and checks the CC fields. For each email address specified in CC, the script checks
that a corresponding account exists in Service Desk. If it doesn't, an account is
created.

The script then passes the email over to Service Desk for further handling.

The script does not have to run on the same server as JIRA Service Desk but it may
make the configuration simpler if it does. An explanation and configuration requirements
are given below as to a possible way to implement the necessary email routing.

# Email Flow
In order for this to work, email that would normally go directly to Service Desk
has to go to the script first. So, instead of:

User sends email -> JIRA Service Desk

we have:

User sends email -> CC script -> JIRA Service Desk

One way to do this is to have the email sent from the user routed directly to the
script. This can be done by running the script on the same server and intercepting
the incoming email with a local alias, e.g.:

support: "|/srv/servicedesk-cc/servicedesk-cc.py -a support"

The Mappings section of the script's configuration file controls how the email is
passed on to Service Desk. The "-a" parameter specifies which alias to use from the
Mappings section and the right-hand side of that line specifies the email address
to use to deliver the email on to Service Desk.

For example:

support: local-support@localhost

would match onto the previous alias entry and cause the script to send the
incoming email on to "local-support@localhost". This works really well if Service
Desk is running on the same server and using a local IMAP/POP3 service to collect
the emails.

# Configuration
The configuration file has three sections: JIRA, Misc and Mappings

## JIRA
This section provides the details that the script requires in order to connect
to Service Desk.

*jira-url* is the base URL for ServiceDesk, e.g.:

jira-url: https://servicedesk.example.com

*jira-user* is the username for the account to be used when creating new user
accounts. It has to have sufficient privilege *in JIRA* to do that.

*jira-password* is the password for the aforementioned JIRA user account.

## Misc
This section specifies the hostname of the SMTP server to be used. At this time,
the script only supports unauthenticated SMTP sending.

## Mappings
This section tells the script how to pass on incoming emails as previously explained.

Each line of the Mappings section consists of an alias on the left hand side and an
email address on the right hand side. The alias can be anything you like but *must*
match the alias specified after "-a" when the script is run.
