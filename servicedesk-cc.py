#!/usr/bin/python

import sys
import email
import requests
from requests.auth import HTTPBasicAuth
import json
import smtplib
import syslog
import ConfigParser
import getopt
import os

def CheckServiceDesk(cc):
    # We get passed a 2-tuple of full name and email address
    # Use the email address as the username
    r = requests.get("%s/rest/api/2/user?username=%s" % (jira_url, cc[1]), auth=jira_auth)
    if r.status_code == 404:
        # Create the account
        name = cc[0]
        if name == "":
            name = cc[1]
        acc = {"name": cc[1], "emailAddress": cc[1], "displayName": name}
        json_acc = json.dumps(acc)
        r = session.post("%s/rest/api/2/user" % jira_url, headers=headers, data=json_acc, auth=jira_auth)
        if r.status_code != 201:
            syslog.syslog("Creation of account for %s failed with code %s and message %s" % (cc[1], r.status_code, r.text))

############################################################################################################################

def main (argv):
    global headers
    global session
    global jira_url
    global jira_auth
    
    headers = {'content-type': 'application/json'}
    session = requests.Session()

    config = ConfigParser.ConfigParser()
    dirPath = os.path.dirname(os.path.realpath(__file__))
    config.read('%s/servicedesk-cc.config' % dirPath)
    smtpServer = config.get("Misc", "smtp-server", "localhost")
    jira_url = config.get("JIRA", "jira-url", "")
    jira_user = config.get("JIRA", "jira-user", "")
    jira_password = config.get("JIRA", "jira-password", "")

    if jira_url == "" or jira_user == "" or jira_password == "":
        syslog.syslog("Invalid configuration of JIRA parameters")
        quit()

    jira_auth = HTTPBasicAuth(jira_user, jira_password)

    ###
    #
    # Get the email address associated with the incoming email. We have to do it this way
    # to avoid potential problems caused by people who might decide to blind-copy the support
    # system, since that would only be in the email envelope which we don't get to see.
    #
    ###

    try:
        opts, args = getopt.getopt(argv, "a:")
    except getopt.GetoptError:
        syslog.syslog("Error: servicedesk-cc -a <alias>")
        quit()
        
    alias = ""
    for opt, arg in opts:
        if opt == "-a":
            alias = arg

    if alias == "":
        syslog.syslog("Error: alias is misconfigured. Must specify -a <alias>")
        quit()
        
    ###
    #
    # Need to match (one of) the To addresses in the email address mapping file
    # so that we know where to send on the email afterwards.
    #
    ###

    newTo = config.get("Mappings", alias, "")
    if newTo == "":
        syslog.syslog("Couldn't find '%s' in Mappings config" % alias)
        quit()

    ###
    #
    # Read the incoming email
    #
    ###

    email_input = sys.stdin.readlines()
    parser = email.FeedParser.FeedParser()
    msg = None
    for msg_line in email_input:
        parser.feed(msg_line)
    msg = parser.close()

    ###
    #
    # msg holds the complete email Message object. Now go through
    # the CC's and check that they exist in JIRA.
    #
    ###
    ccs = msg.get_all('cc', [])
    if ccs <> []:
        addresses = email.utils.getaddresses(ccs)
        for addr in addresses:
            CheckServiceDesk(addr)

    ###
    #
    # Pass the email over to Service Desk
    #
    ###
    s = smtplib.SMTP(smtpServer)
    s.sendmail(msg['From'], newTo, msg.as_string())
    s.quit()

if __name__ == "__main__":
   main(sys.argv[1:])
